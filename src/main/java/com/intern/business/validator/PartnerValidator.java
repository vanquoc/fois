package com.intern.business.validator;

import com.intern.business.model.api.Partner;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class PartnerValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Partner.class.equals(aClass);
    }

    @Override
        public void validate(Object o, Errors errors) {

    }
}
