package com.intern.business.service;


import com.intern.business.model.web.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

	List<User> findAll();
	
	Optional<User> findOne(Integer id);
	
	long countAll();
	
	void delete(Integer id);
	
	com.intern.business.model.api.User checkLogin(com.intern.business.model.api.User user);
	
	boolean register(User user);
	
}
