package com.intern.business.service;

import com.intern.business.model.api.Partner;

import java.util.List;
import java.util.Optional;

public interface PartnerService {

    List<Partner> findAll();

    Optional<Partner> findOne(Long id);

    Partner create(Partner partner);

    boolean update(Partner partner);

    long countAll();

    boolean delete(Long id);

}
