package com.intern.business.service;

import com.intern.business.model.api.Partner;
import com.intern.business.repository.PartnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PartnerServiceImpl implements PartnerService {

    @Autowired
    PartnerRepository partnerRepository;

    @Override
    public List<Partner> findAll() {
        return partnerRepository.findAll();
    }

    @Override
    public Optional<Partner> findOne(Long id) {
        return partnerRepository.findById(id);
    }

    @Override
    public Partner create(Partner partner) {
        return partnerRepository.save(partner);
    }

    @Override
    public boolean update(Partner partner) {
        if (partnerRepository.findById(partner.getPartnerID()).isPresent()) {
            partnerRepository.save(partner);
            return true;
        }
        return false;
    }

    @Override
    public long countAll() {
        return partnerRepository.count();
    }

    @Override
    public boolean delete(Long id) {
        if(!partnerRepository.findById(id).isPresent()){
            return false;
        }

        partnerRepository.deleteById(id);
        return true;
    }
}
