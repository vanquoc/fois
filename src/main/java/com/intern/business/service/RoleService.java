package com.intern.business.service;


import com.intern.business.model.web.Role;

public interface RoleService {

	Role findByName(String name);
	
}
