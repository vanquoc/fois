package com.intern.business.model.api;

import com.intern.business.model.api.Audit.DateAudit;
import org.apache.tomcat.jni.Local;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "partner")
public class Partner extends DateAudit {
    @Id
    @Column(length = 10, name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long PartnerID;

    @NotBlank
    @Column(name = "company_name")
    private String CompanyName;

    @NotNull
    @Column(name = "number_of_employees")
    private int NumberOfEmployees;

    @NotNull
    @Column(name = "capital")
    private long Capital;

    @NotBlank
    @Column(name = "home_page")
    private String HomePage;

    @NotBlank
    @Column(name = "phone_number")
    private String PhoneNumber;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "founded")
    private LocalDate Founded;

    @NotBlank
    @Column(name = "email")
    private String Email;

    @Column(columnDefinition = "text", name = "business_content")
    @NotBlank
    private String BusinessContent;

    @NotBlank
    @Column(name = "rating")
    private String Rating;

    @NotBlank
    @Column(name = "achievement")
    private String Achievement;

    public Long getPartnerID() {
        return PartnerID;
    }

    public void setPartnerID(Long partnerID) {
        PartnerID = partnerID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public int getNumberOfEmployees() {
        return NumberOfEmployees;
    }

    public void setNumberOfEmployees(int numberOfEmployees) {
        NumberOfEmployees = numberOfEmployees;
    }

    public long getCapital() {
        return Capital;
    }

    public void setCapital(long capital) {
        Capital = capital;
    }

    public String getHomePage() {
        return HomePage;
    }

    public void setHomePage(String homePage) {
        HomePage = homePage;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public LocalDate getFounded() {
        return Founded;
    }

    public void setFounded(LocalDate founded) {
        Founded = founded;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getBusinessContent() {
        return BusinessContent;
    }

    public void setBusinessContent(String businessContent) {
        BusinessContent = businessContent;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getAchievement() {
        return Achievement;
    }

    public void setAchievement(String achievement) {
        Achievement = achievement;
    }

}
