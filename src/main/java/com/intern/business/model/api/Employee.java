package com.intern.business.model.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.intern.business.model.api.Audit.DateAudit;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "employee")
public class Employee extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long EmployeeID;

    @NotBlank
    @Column(name = "department")
    private String Department;

    @NotBlank
    @Column(name = "position")
    private String Position;

    @NotBlank
    @Column(name = "firstname")
    private String FirstName;

    @NotBlank
    @Column(name = "lastname")
    private String LastName;

    @NotBlank
    @Column(name = "email")
    private String Email;

    @NotBlank
    @Column(name = "phonenumber")
    private String PhoneNumber;

    public Long getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        EmployeeID = employeeID;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String position) {
        Position = position;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }
}