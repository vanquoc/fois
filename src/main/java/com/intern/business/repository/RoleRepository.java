package com.intern.business.repository;

import com.intern.business.model.web.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Integer> {
	
	Role findByName(String name);

}
