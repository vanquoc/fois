package com.intern.business.controller.api;

import com.intern.business.model.api.Partner;
import com.intern.business.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class ApiPartnerController {

    @Autowired
    PartnerService partnerService;

    //Get all partner
    @GetMapping("/api/v1/partner")
    public ResponseEntity<List<Partner>> getAllPartner(){
        List<Partner> partners = partnerService.findAll();
        return new ResponseEntity<>(partners, HttpStatus.OK);
    }

    //Add a partmer
    @PostMapping("/api/v1/partner")
    public ResponseEntity<Partner> createPartner(@Valid @RequestBody Partner partner) {
        return new ResponseEntity<>(partnerService.create(partner), HttpStatus.OK);
    }

    //Get a partner by id
    @PostMapping("/api/v1/partner/{id}")
    public ResponseEntity<Partner> getPartnerById(@PathVariable(value = "id") Long partnetID) {
        if (partnetID == null) {
            return new ResponseEntity<>((Partner) null, HttpStatus.NOT_FOUND);
        }
        Optional<Partner> partner = partnerService.findOne(partnetID);

        if(partner.isPresent()){
            return new ResponseEntity<>(partner.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>((Partner) null, HttpStatus.NOT_FOUND);
    }

    //Update a partner
    @PutMapping("/api/v1/partner/{id}")
    public ResponseEntity<Boolean> updatePartner(@PathVariable(value = "id") Long partnetID, @Valid @RequestBody Partner partnerDetail) {
        Optional<Partner> partner = partnerService.findOne(partnetID);

        if (!partner.isPresent()) {
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }

        partnerDetail.setPartnerID(partnetID);
        partnerService.update(partnerDetail);

        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    //Delete a partner
    @DeleteMapping("/api/v1/partner/{id}")
    public ResponseEntity<Boolean> deletePartner(@PathVariable(value = "id") Long partnerID) {
        Optional<Partner> partner = partnerService.findOne(partnerID);

        if (!partner.isPresent()) {
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }

        partnerService.delete(partnerID);
        return new ResponseEntity<>(false, HttpStatus.OK);
    }
}
