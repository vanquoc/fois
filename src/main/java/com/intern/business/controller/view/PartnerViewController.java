package com.intern.business.controller.view;

import com.intern.business.model.api.Partner;
import com.intern.business.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

@Controller
public class PartnerViewController {
    static Logger log = Logger.getLogger(PartnerViewController.class.getName());

    @Autowired
    PartnerService partnerService;

    @ModelAttribute(value = "partner")
    public Partner newEntity()
    {
        return new Partner();
    }

    @GetMapping("/admin/partner")
    public ModelAndView index(Model model) {
        ModelAndView mav = new ModelAndView("admin/partner");
        mav.addObject("partners", partnerService.findAll());
        return mav;
    }

    @PostMapping("/admin/partner/save")
    public ModelAndView save(@Valid Partner partner, BindingResult result) {
        ModelAndView mav = new ModelAndView("redirect:/admin/partner");
        if (result.hasErrors()) {
            log.info("Binding have error");
            log.info(result.getAllErrors().toString());
            mav.addObject("partners", partnerService.findAll());
            mav.setViewName("admin/partner");
            return mav;
        }
        partnerService.create(partner);
        mav.addObject("partners", partnerService.findAll());
//        redirect.addFlashAttribute("success", "Lưu đối tác thành công!");
        return mav;
    }


}
