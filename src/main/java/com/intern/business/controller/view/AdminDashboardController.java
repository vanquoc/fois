package com.intern.business.controller.view;

import com.intern.business.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class AdminDashboardController {

	@Autowired
	private UserService userService;

	
	@GetMapping({"/admin", "/admin/dashboard"})
	public String index(Model model) {
		// Stats
		long totalUsers = userService.countAll();
		
		model.addAttribute("totalUsers", totalUsers);
		return "admin/dashboard";
	}

	@GetMapping("/admin/international")
	public String getInternationalPage() {

		return "admin/international";
	}
	
}
