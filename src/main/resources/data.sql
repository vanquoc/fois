INSERT INTO `role` (`id`, `name`) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_CUSTOMER');


INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(1, 'Pham Hung Vy', 'shark@gmail.com', '$2a$10$8rZSc1LmxNFbhk2InpJzw.FHh7V7.egp3fCqJnRt3wnEbcOXyNyu2');


INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2);

INSERT INTO `partner`
(`company_name`, `capital`, `founded`, `number_of_employees`, `home_page`, `email`, `phone_number` , `business_content`, `achievement`, `rating` ) 
VALUES ('ABC company', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good'),
('ABC company2', 500000,DATE '2002-01-01', 100, 'https://google.com', 'support@gmail.com', '0123221321', 'Advertiment', 'None', 'Good')